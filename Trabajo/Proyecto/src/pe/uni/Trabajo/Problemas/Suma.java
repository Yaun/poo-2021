package pe.uni.Trabajo.Problemas;

import java.util.Scanner;

public class Suma {
    private int valor1;
    private int valor2;
    public int getValor1() {
        return valor1;
    }
    public void setValor1(int valor1) {
        this.valor1 = valor1;
    }
    public int getValor2() {
        return valor2;
    }
    public void setValor2(int valor2) {
        this.valor2 = valor2;
    }

    public static void main(String[] args){

        Suma sumar = new Suma();
        Scanner input = new Scanner(System.in);
        System.out.println("Ingrese el primer numero:");
        sumar.setValor1(input.nextInt());

        System.out.println("Ingrese el segundo numero:");
        sumar.setValor2(input.nextInt());

        System.out.println("La suma es: " + (sumar.getValor1()+sumar.getValor2()));
    }
    
}
