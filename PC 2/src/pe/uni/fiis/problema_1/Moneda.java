package pe.uni.fiis.problema_1;

public class Moneda {
    private String nombreMoneda;
    private double tipocambioDolares;
    
    public Moneda(String nombreMoneda, double tipocambioDolares) {
        this.nombreMoneda = nombreMoneda;
        this.tipocambioDolares = tipocambioDolares;
    }

    public double ToDolar(double cantidad){
        return this.tipocambioDolares*cantidad;
    }

    public double DolarTo(double cantidad){
        return cantidad/this.tipocambioDolares;
    }

}
