package pe.uni.fiis.problema_1;

public class Aplicacion {
    
    public static void main(String[] args){
        double valor=0;
        double cantidadmonedas = 2;

        Moneda sol = new Moneda("Soles", 3.57);
        Moneda yen = new Moneda("Yenes", 4.2);

        valor = yen.ToDolar(cantidadmonedas);

        System.out.println(sol.DolarTo(valor));

    }
    
}
