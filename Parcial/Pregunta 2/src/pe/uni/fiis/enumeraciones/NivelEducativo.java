package pe.uni.fiis.enumeraciones;

public enum NivelEducativo {
    PRIMARIA("PRIMARIA"), SECUNDARIA("SECUNDARIA"), SUPERIOR("SUPERIOR");

    private String nivelEducativo;

    NivelEducativo(String nivelEducativo){
        this.nivelEducativo = nivelEducativo;
    }

    public String getNivelEducativo() {
        return nivelEducativo;
    }

}
