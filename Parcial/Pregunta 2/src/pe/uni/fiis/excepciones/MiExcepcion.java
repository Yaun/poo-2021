package pe.uni.fiis.excepciones;

public class MiExcepcion extends Exception{

    public MiExcepcion(String message) {
        super(message);
    }
}
