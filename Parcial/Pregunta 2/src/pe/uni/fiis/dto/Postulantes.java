package pe.uni.fiis.dto;

import pe.uni.fiis.enumeraciones.NivelEducativo;

public class Postulantes {
    private String nombre;
    private String apellidos;
    private String direccion;
    private Integer dni;
    private Integer telefono;
    private Integer edad;
    private Integer notaEvaluacion;
    private NivelEducativo nivel;

    public Postulantes(String nombre, String apellidos, String direccion, 
            Integer dni, Integer telefono, Integer edad,
            Integer notaEvaluacion, NivelEducativo nivel) {

        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.dni = dni;
        this.telefono = telefono;
        this.edad = edad;
        this.notaEvaluacion = notaEvaluacion;
        this.nivel = nivel;
    }

    
}
