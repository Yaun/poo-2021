package pe.uni.fiis.aplicando;

import pe.uni.fiis.dto.Libros;
import pe.uni.fiis.servicio.ImpService;
import pe.uni.fiis.servicio.Servicio;

import java.util.*;

public class Aplicacion {
    public static void main(String[] args){
        List<Libros> biblioteca = new ArrayList<>();
        Libros libro = new Libros();
        libro.setAutor("Eduardo");
        libro.setCodigo(1);
        libro.setTitulo("Batman");

        Libros libro1 = new Libros();
        libro.setAutor("José");
        libro.setCodigo(2);
        libro.setTitulo("Superman");

        Libros libro2 = new Libros();
        libro.setAutor("Mario");
        libro.setCodigo(3);
        libro.setTitulo("Aquaman");

        biblioteca.add(libro);
        biblioteca.add(libro1);
        biblioteca.add(libro2);

        Servicio servicio = new ImpService();
        List<Libros> lista1 = servicio.buscarLibroAutor(biblioteca, "José");
        for (Libros libros : lista1) {
            System.out.println(libros.getAutor());
        }
        
    }
}
