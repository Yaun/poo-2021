package pe.uni.fiis.servicio;
import pe.uni.fiis.dto.Libros;

import java.util.*;

public interface Servicio {
    List<Libros> buscarLibroTitulo(List<Libros> lista,String titulo);
    List<Libros> buscarLibroAutor(List<Libros> lista,String autor);
    List<Libros> buscarLibroCodigo(List<Libros> lista,Integer codigo);
    
}
