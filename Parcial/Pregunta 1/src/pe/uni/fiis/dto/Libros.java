package pe.uni.fiis.dto;

import pe.uni.fiis.abstracts.LibroAbstracto;

public class Libros extends LibroAbstracto{

    private String descripcion;
    private String editorial;
    private Integer añoPublicacion;
    private String tituloCapitulos;
    private String resumen;
    
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getEditorial() {
        return editorial;
    }
    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
    public Integer getAñoPublicacion() {
        return añoPublicacion;
    }
    public void setAñoPublicacion(Integer añoPublicacion) {
        this.añoPublicacion = añoPublicacion;
    }
    public String getTituloCapitulos() {
        return tituloCapitulos;
    }
    public void setTituloCapitulos(String tituloCapitulos) {
        this.tituloCapitulos = tituloCapitulos;
    }
    public String getResumen() {
        return resumen;
    }
    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

}

