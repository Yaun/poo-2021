import java.util.Scanner;

public class Aplicacion {
    public static void main(String[] args){
        int cantidad;
        double modulo;
        double apotema;

        Scanner input = new Scanner(System.in);
        
        System.out.println("Hallar el área, perímetro y diagonales de un polinomio");
        System.out.println("Ingrese el número de lados: ");
        do{
            cantidad = input.nextInt();
            if(cantidad<1 || cantidad>6){
                System.out.println("Error, numero de lados incorrecto");
                System.out.println("Debe ser mayor a 0 y menor a 7");
            }
        }while(cantidad<1 || cantidad>6);

        System.out.println("Ingrese el valor de los lados: ");
        do{
            modulo = input.nextInt();
            if(modulo <1){
                System.out.println("Error, cantidad incorrecta");
                System.out.println("Debe ser positivo");
            }
        }while(modulo <1);
        
        
        Area areapoligono = new Area();
        apotema = areapoligono.hallarapotema(cantidad);
        System.out.println("El area es: " + areapoligono.hallarArea(cantidad, modulo,apotema));
        
        Perimetro perimetropoligono = new Perimetro();
        System.out.println("El perímetro es: " + perimetropoligono.hallarperimetro(cantidad, modulo));

    }
}
