import java.nio.DoubleBuffer;

public class Perimetro {
    private int lado;
    private DoubleBuffer valor;
    private double perimetro;

    public double hallarperimetro(int lado, double valor){
        perimetro = lado*valor;
        return perimetro;
    }
}
