public class Area {
    private int lado;
    private double area;
    private double valor;
    private double apotema;
    
    public double hallarapotema(int lado){
        apotema = this.lado / (2*Math.tan((360/((2*this.lado)*Math.PI)/180)));
        return apotema;
    }

    public double hallarArea (int lado, double valor, double apotema){
        area = ((lado*valor)*apotema)/2;
        return area;
    }
}
