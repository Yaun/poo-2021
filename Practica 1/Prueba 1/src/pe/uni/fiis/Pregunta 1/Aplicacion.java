public class Aplicacion {

    public static void main(String[] args){
        Cliente cliente1 = new Cliente();
        Cliente cliente2 = new Cliente();
        
        Calzado calzado1 = new Calzado();
        Calzado calzado2 = new Calzado();
        
        //Selecciono genero de los clientes

        cliente1.setGenero("Masculino");
        cliente2.setGenero("Femenino");

        //Le doy los valores al calzado 1

        calzado1.setGenero("M");
        calzado1.setHorma(34);
        calzado1.setMarca("Nike");
        calzado1.setMaterial("Cuero");
        calzado1.setModelo("AE189");
        calzado1.setPaisOrigen("Perú");
        calzado1.setTemporada("Otoño");
        calzado1.setTipo("Vestir");

        //Le doy los valores al calzado 1

        calzado2.setGenero("F");
        calzado2.setHorma(28);
        calzado2.setMarca("Nike");
        calzado2.setMaterial("Cuero");
        calzado2.setModelo("KJ539");
        calzado2.setPaisOrigen("Perú");
        calzado2.setTemporada("Otoño");
        calzado2.setTipo("Vestir");

        System.out.println("EL cliente 1 de genero: " + cliente1.getGenero() + ", compró: ");
        System.out.println("Caracterizticas: ");
        System.out.println("Género: " + calzado1.getGenero());
        System.out.println("Horma: "+calzado1.getHorma());
        System.out.println("Marca: "+calzado1.getMarca());
        System.out.println("Material: "+calzado1.getMaterial());
        System.out.println("Modelo: "+calzado1.getModelo());
        System.out.println("Pais de origen: "+calzado1.getPaisOrigen());
        System.out.println("Temporada: "+calzado1.getTemporada());
        System.out.println("Tipo: "+calzado1.getTipo());


        System.out.println("EL cliente 2 de genero: " + cliente2.getGenero() + ", compró: ");
        System.out.println("Caracterizticas: ");
        System.out.println("Género: " + calzado2.getGenero());
        System.out.println("Horma: "+calzado2.getHorma());
        System.out.println("Marca: "+calzado2.getMarca());
        System.out.println("Material: "+calzado2.getMaterial());
        System.out.println("Modelo: "+calzado2.getModelo());
        System.out.println("Pais de origen: "+calzado2.getPaisOrigen());
        System.out.println("Temporada: "+calzado2.getTemporada());
        System.out.println("Tipo: "+calzado2.getTipo());
  
    }

}
