public class Calzado {
    
    private String marca;
    private String temporada;
    private String modelo;
    private String tipo;
    private String genero;
    private String material;
    private int horma;
    private String paisOrigen;

    //Getter and Setters
    
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public int getHorma() {
        return horma;
    }

    public void setHorma(int horma) {
        this.horma = horma;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public String getTemporada() {
       return temporada;
    }

    public void setTemporada(String temporada) {
        this.temporada = temporada;
    }

}
