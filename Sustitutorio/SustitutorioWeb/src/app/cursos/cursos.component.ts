import { Component, OnInit } from '@angular/core';
import {Curso} from "../Interfaces";
import {ApiService} from "../ApiService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {
  nombre: String = '';
  fecha_inicio: String = '';
  fecha_fin: String = '';
  precio:number = 0;
  lista: Curso[] = [];
  listaCurso: Curso[] = [];
  constructor(private api: ApiService,  private route: Router) { }

  ngOnInit(): void {

  }
  mostrar(): void {
    const curso: Curso = {
    id: undefined,
    nombre: this.nombre,
    precio: this.precio,
    fecha_inicio: this.fecha_inicio,
    fecha_fin: this.fecha_fin
  }
  this.api.obtenerCursos(curso).subscribe( respuesta =>{
    this.lista = respuesta.lista;
  });
 }


}
