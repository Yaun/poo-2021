export interface Estudiante{
  id?: number;
  nombres?: string;
  apellidos?: string;
  correo?: string;
  clave?: string;
}

export interface Curso {
  id?: number;
  nombre?: string;
  precio?: number;
  fecha_inicio?: string;
  fecha_fin?: string;
}
export interface RespuestaCurso {
  lista: Curso[];
}
