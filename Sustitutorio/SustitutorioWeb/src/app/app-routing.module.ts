import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AutenticarComponent} from "./autenticar/autenticar.component";
import {CursosComponent} from "./cursos/cursos.component";

const routes: Routes = [
  {path: "autenticar", component: AutenticarComponent},
  {path: "cursos", component: CursosComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
