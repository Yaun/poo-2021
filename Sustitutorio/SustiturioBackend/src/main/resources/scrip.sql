CREATE TABLE estudiante(
                           id_estudiante NUMERIC(9) PRIMARY KEY,
                           nombres VARCHAR(100),
                           apellidos VARCHAR(100),
                           correo VARCHAR(200),
                           clave VARCHAR(200)
);

CREATE TABLE inscripcion_curso(
                                  id_curso NUMERIC(9) PRIMARY KEY,
                                  id_estudiante NUMERIC(9),
                                  fecha_inscripcion VARCHAR(10)
);

CREATE TABLE curso(
                      id_curso NUMERIC(9) PRIMARY KEY,
                      nombre VARCHAR(200),
                      precio NUMERIC(9,2),
                      fecha_inicio VARCHAR(10),
                      fecha_fin VARCHAR(10)
);

DROP TABLE inscripcion_curso;
SELECT *
FROM curso;

ALTER TABLE inscripcion_curso ADD CONSTRAINT FOREIGN KEY (id_curso) REFERENCES curso(id_curso);
ALTER TABLE inscripcion_curso ADD CONSTRAINT FOREIGN KEY (id_estudiante) REFERENCES estudiante(id_estudiante);
ALTER TABLE inscripcion_curso ADD primary KEY (id_estudiante);

CREATE SEQUENCE seq_estudiante START WITH 1;
CREATE SEQUENCE seq_curso START WITH 1;

INSERT INTO estudiante(id_estudiante ,nombres ,apellidos ,correo ,clave)
VALUES (NEXTVAL(seq_estudiante),'Breimer Paul','Palacios Sinche','breimer@gmail.com','aracely');

INSERT INTO estudiante(id_estudiante ,nombres ,apellidos ,correo ,clave)
VALUES (NEXTVAL(seq_estudiante),'Gianpiere','Reto Caycho','reto@gmail.com','julian');

INSERT INTO estudiante(id_estudiante ,nombres ,apellidos ,correo ,clave)
VALUES (NEXTVAL(seq_estudiante),'Jean Brian','Yaun Espada','yaun@gmail.com','hrdtxpln');

INSERT INTO estudiante(id_estudiante ,nombres ,apellidos ,correo ,clave)
VALUES (NEXTVAL(seq_estudiante),'Edmundo','Huicho','huichin@gmail.com','daniela');


INSERT INTO curso(id_curso, nombre, precio, fecha_inicio, fecha_fin)
VALUES (NEXTVAL(seq_curso), 'Fisica', 28.5, '2021-08-10','2021-09-15');

INSERT INTO curso(id_curso, nombre, precio, fecha_inicio, fecha_fin)
VALUES (NEXTVAL(seq_curso), 'Calculo', 26.8, '2021-08-14','2021-09-20');

INSERT INTO curso(id_curso, nombre, precio, fecha_inicio, fecha_fin)
VALUES (NEXTVAL(seq_curso), 'Quimica', 30.5, '2021-08-20','2021-10-01');

INSERT INTO curso(id_curso, nombre, precio, fecha_inicio, fecha_fin)
VALUES (NEXTVAL(seq_curso), 'Base de datos', 38.5, '2021-08-10','2021-09-10');


INSERT INTO inscripcion_curso(id_curso,id_estudiante, fecha_inscripcion)
VALUES (1,1,'2021-08-15');

INSERT INTO inscripcion_curso(id_curso,id_estudiante, fecha_inscripcion)
VALUES (2,2,'2021-08-20');

INSERT INTO inscripcion_curso(id_curso,id_estudiante, fecha_inscripcion)
VALUES (3,3,'2021-08-20');

INSERT INTO inscripcion_curso(id_curso,id_estudiante, fecha_inscripcion)
VALUES (4,4,'2021-08-12');