package com.fiis.sustitutoriob.dto;

import lombok.Data;

import java.util.List;

@Data
public class RespuestaCurso {
    private List<Curso> lista;
}
