package com.fiis.sustitutoriob.dto;

import lombok.Data;

@Data
public class Estudiante {
    private Integer id_usuario;
    private String nombres;
    private String apellidos;
    private String correo;
    private String clave;

    public Estudiante(Integer id_usuario, String nombres, String apellidos, String correo, String clave) {
        this.id_usuario = id_usuario;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.clave = clave;
    }
}
