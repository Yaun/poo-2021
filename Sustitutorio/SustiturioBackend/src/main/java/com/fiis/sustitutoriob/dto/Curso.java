package com.fiis.sustitutoriob.dto;

import lombok.Data;

@Data
public class Curso {
    private Integer id_curso;
    private String nombre;
    private Double precio;
    private String fecha_inicio;
    private String fecha_fin;

    public Curso(Integer id_curso, String nombre, Double precio, String fecha_inicio, String fecha_fin) {
        this.id_curso = id_curso;
        this.nombre = nombre;
        this.precio = precio;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
    }
}
