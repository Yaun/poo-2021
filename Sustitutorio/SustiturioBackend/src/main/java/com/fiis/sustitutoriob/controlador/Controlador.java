package com.fiis.sustitutoriob.controlador;


import com.fiis.sustitutoriob.dto.Curso;
import com.fiis.sustitutoriob.dto.Estudiante;
import com.fiis.sustitutoriob.dto.RespuestaCurso;
import com.fiis.sustitutoriob.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(value={"*"})
public class Controlador {

    @Autowired
    private Servicio servicio;

    @RequestMapping(
            value = "autenticar-estudiante", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    Curso autenticarEstudiante(@RequestBody Estudiante estudiante){
        return servicio.autenticarEstudiante(estudiante);
    }

    @RequestMapping(
            value = "mostrar-cursos", method = RequestMethod.POST,
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    RespuestaCurso obtenerCursos(){
        RespuestaCurso respuestaCurso = new RespuestaCurso();
        respuestaCurso.setLista(servicio.obtenerCursos());
        return respuestaCurso;
    }
}
