package com.fiis.sustitutoriob.servicio;


import com.fiis.sustitutoriob.dao.Dao;
import com.fiis.sustitutoriob.dto.Curso;
import com.fiis.sustitutoriob.dto.Estudiante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{

    @Autowired
    private Dao dao;

    public Curso autenticarEstudiante(Estudiante estudiante) {
        return dao.autenticarEstudiante(estudiante);
    }

    public List<Curso> obtenerCursos() {
        return dao.obtenerCursos();
    }
}
