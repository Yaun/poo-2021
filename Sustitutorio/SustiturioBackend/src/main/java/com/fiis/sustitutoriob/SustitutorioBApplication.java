package com.fiis.sustitutoriob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SustitutorioBApplication {

    public static void main(String[] args) {
        SpringApplication.run(SustitutorioBApplication.class, args);
    }

}
