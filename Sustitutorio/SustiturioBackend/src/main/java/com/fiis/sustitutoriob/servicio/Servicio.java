package com.fiis.sustitutoriob.servicio;

import com.fiis.sustitutoriob.dto.Curso;
import com.fiis.sustitutoriob.dto.Estudiante;

import java.util.List;

public interface Servicio {
    public Curso autenticarEstudiante(Estudiante estudiante);
    public List<Curso> obtenerCursos();
}
