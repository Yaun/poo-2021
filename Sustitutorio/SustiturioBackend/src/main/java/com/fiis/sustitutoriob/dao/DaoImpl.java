package com.fiis.sustitutoriob.dao;


import com.fiis.sustitutoriob.dto.Curso;
import com.fiis.sustitutoriob.dto.Estudiante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{

    @Autowired
    JdbcTemplate jdbcTemplate;
    Connection connection;

    private void obtenerConexion(){
        try {
            connection=jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion(){
        try {
            this.connection.commit();
            this.connection.close();
            this.connection = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    private void cerrarConexionStatement(ResultSet resultSet, Statement statement){
        try {
            if(resultSet != null) resultSet.close();
            if(statement != null) statement.close();
            this.connection.commit();
            this.connection.close();
            this.connection = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }private void cerrarConexionPreparetatement(ResultSet resultSet, PreparedStatement preparedStatement){
        try {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            this.connection.commit();
            this.connection.close();
            this.connection = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Curso autenticarEstudiante(Estudiante estudiante) {
        String sql = " SELECT c.nombre, c.precio, c.fecha_inicio, c.fecha_fin\n" +
                " FROM curso c\n" +
                " JOIN inscripcion_curso i\n" +
                " ON i.id_curso = c.id_curso\n" +
                " JOIN estudiante e\n" +
                " ON e.id_estudiante = i.id_estudiante\n" +
                " WHERE e.correo = ? and e.clave = ?";
        Curso cursof = null;
        try {
            obtenerConexion();
            PreparedStatement preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, estudiante.getCorreo());
            preparedStatement.setString(2, estudiante.getClave());

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
               cursof = extraerCurso(resultSet);
            };
            cerrarConexionPreparetatement(resultSet, preparedStatement);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        
        return cursof;
    }

    public List<Curso> obtenerCursos(){
        List<Curso> lista = new ArrayList<>();
        String sql = "SELECT nombre, precio, fecha_inicio, fecha_fin\n" +
                " FROM curso";

        try {
            obtenerConexion();
            Statement statement = this.connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while(resultSet.next()){
                lista.add(extraerCurso(resultSet));
            }
            cerrarConexionStatement(resultSet,statement);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return lista;
    }


    private Curso extraerCurso(ResultSet resultSet) throws SQLException{
        Curso curso = new Curso(
                null,
                resultSet.getString("nombre"),
                resultSet.getDouble("precio"),
                resultSet.getString("fecha_inicio"),
                resultSet.getString("fecha_fin")
        );
        return curso;
    }
}
