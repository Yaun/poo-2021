package com.fiis.sustitutoriob.dao;

import com.fiis.sustitutoriob.dto.Curso;
import com.fiis.sustitutoriob.dto.Estudiante;

import java.util.List;

public interface Dao {
    public Curso autenticarEstudiante(Estudiante estudiante);
    public List<Curso> obtenerCursos();
}
