package com.examen.poo.dao;

import com.examen.poo.dto.Usuario;

import java.util.List;

public interface Dao {
    public Usuario auntenticarUsuario(Usuario usuario);
    public List<Usuario> busquedaUsuarioNombre(Usuario usuario);
    public List<Usuario> busquedaUsuarioApellidos(Usuario usuario);
    public List<Usuario> busquedaUsuarioCorreo(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
}
