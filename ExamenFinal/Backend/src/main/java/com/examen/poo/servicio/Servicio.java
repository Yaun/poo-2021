package com.examen.poo.servicio;

import com.examen.poo.dto.Usuario;

import java.util.List;

public interface Servicio {
    public Usuario auntenticarUsuario(Usuario usuario);
    public List<Usuario> busquedaUsuarioNombre(Usuario usuario);
    public List<Usuario> busquedaUsuarioApellidos(Usuario usuario);
    public List<Usuario> busquedaUsuarioCorreo(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
}
