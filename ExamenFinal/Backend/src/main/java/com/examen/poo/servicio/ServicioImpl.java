package com.examen.poo.servicio;

import com.examen.poo.dao.Dao;
import com.examen.poo.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;

    public Usuario auntenticarUsuario(Usuario usuario) {
        return dao.auntenticarUsuario(usuario);
    }

    public List<Usuario> busquedaUsuarioNombre(Usuario usuario) {
        return dao.busquedaUsuarioNombre(usuario);
    }

    public Usuario actualizarUsuario(Usuario usuario) {
        return dao.actualizarUsuario(usuario);
    }

    public List<Usuario> busquedaUsuarioApellidos(Usuario usuario){
        return dao.busquedaUsuarioApellidos(usuario);
    }

    public List<Usuario> busquedaUsuarioCorreo(Usuario usuario){
        return dao.busquedaUsuarioCorreo(usuario);
    }
}
