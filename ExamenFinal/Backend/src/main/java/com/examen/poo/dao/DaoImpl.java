package com.examen.poo.dao;

import com.examen.poo.dto.RespuestaUsuario;
import com.examen.poo.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Repository
public class DaoImpl implements Dao{

    @Autowired
    JdbcTemplate jdbcTemplate;
    Connection connection;

    private void obtenerConexion(){
        try {
            connection=jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexionStatement(ResultSet resultSet, Statement statement){
        try {
            if(resultSet != null) resultSet.close();
            if(statement != null) statement.close();
            this.connection.commit();
            this.connection.close();
            this.connection = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }private void cerrarConexionPreparetatement(ResultSet resultSet, PreparedStatement preparedStatement){
        try {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            this.connection.commit();
            this.connection.close();
            this.connection = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private Usuario extraerUsuario(ResultSet resultSet) throws SQLException{
        Usuario usuario = new Usuario(
            resultSet.getInt("id_usuario"),
            resultSet.getString("nombres"),
            resultSet.getString("apellidos"),
            resultSet.getString("correo"),
            "1".equals(resultSet.getString("administrador")) ? true: false,
            null
        );
        if(usuario.getAdministrador()){
            return null;
        } else{
            return usuario;
        }
    }

    private Usuario verificarAdministrador(ResultSet resultSet) throws SQLException{
        Usuario usuario = new Usuario(
                null,
                resultSet.getString("nombres"),
                resultSet.getString("apellidos"),
                resultSet.getString("correo"),
                "1".equals(resultSet.getString("administrador")) ? true: false,
                null
        );
        if(usuario.getAdministrador()){
            return usuario;
        } else{
            return null;
        }
    }

    public Usuario auntenticarUsuario(Usuario usuario) {

        String sql = " SELECT correo, nombres, apellidos, administrador" +
                " FROM usuario" +
                " WHERE correo = ? and clave = ?";
        try {
            obtenerConexion();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, usuario.getCorreo());
            preparedStatement.setString(2, usuario.getClave());

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                usuario = verificarAdministrador(resultSet);
            }
            cerrarConexionPreparetatement(resultSet, preparedStatement);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }

    public List<Usuario> busquedaUsuarioNombre(Usuario usuario) {
        List<Usuario> lista = new ArrayList<>();

        String sql = " SELECT id_usuario, nombres, apellidos, correo, administrador\n" +
        " FROM usuario\n" +
        " WHERE upper(nombres) like ?";

        try {
            obtenerConexion();
            PreparedStatement preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, "%" +  usuario.getNombres().toUpperCase() + "%");

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Usuario u = extraerUsuario(resultSet);
                if(u != null){lista.add(u);}
            }
            cerrarConexionPreparetatement(resultSet, preparedStatement);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public List<Usuario> busquedaUsuarioApellidos(Usuario usuario){
        List<Usuario> lista = new ArrayList<>();

        String sql = " SELECT id_usuario, nombres, apellidos, correo, administrador\n" +
                " FROM usuario\n" +
                " WHERE upper(apellidos) like ?";

        try {
            obtenerConexion();
            PreparedStatement preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, "%" +  usuario.getApellidos().toUpperCase() + "%");

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Usuario u = extraerUsuario(resultSet);
                if(u != null){lista.add(u);}
            }
            cerrarConexionPreparetatement(resultSet, preparedStatement);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public List<Usuario> busquedaUsuarioCorreo(Usuario usuario){
        List<Usuario> lista = new ArrayList<>();

        String sql = " SELECT id_usuario, nombres, apellidos, correo, administrador\n" +
                " FROM usuario\n" +
                " WHERE upper(correo) like ?";

        try {
            obtenerConexion();
            PreparedStatement preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, "%" +  usuario.getCorreo().toUpperCase() + "%");

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Usuario u = extraerUsuario(resultSet);
                if(u != null){lista.add(u);}
            }
            cerrarConexionPreparetatement(resultSet, preparedStatement);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public Usuario actualizarUsuario(Usuario usuario) {
        String sql = " SELECT id_usuario, nombres, apellidos, correo, administrador\n" +
                " FROM usuario\n" +
                " WHERE upper(correo) like ?";

        try {
            obtenerConexion();
            PreparedStatement preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, "%" +  usuario.getCorreo().toUpperCase() + "%");

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Usuario u = extraerUsuario(resultSet);

            }
            cerrarConexionPreparetatement(resultSet, preparedStatement);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }
}
