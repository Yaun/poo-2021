package com.examen.poo.controlador;

import com.examen.poo.dto.Usuario;
import com.examen.poo.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;

    @RequestMapping(
            value = "verificar-administrador", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    Usuario auntenticarUsuario(@RequestBody Usuario usuario){
        return servicio.auntenticarUsuario(usuario);
    }

    @RequestMapping(
            value = "buscar-usuario-nombres", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    List<Usuario> busquedaUsuarioNombre(@RequestBody Usuario usuario){
        return servicio.busquedaUsuarioNombre(usuario);
    }

    @RequestMapping(
            value = "buscar-usuario-apellidos", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    List<Usuario> busquedaUsuarioApellidos(@RequestBody Usuario usuario){
        return servicio.busquedaUsuarioApellidos(usuario);
    }

    @RequestMapping(
            value = "buscar-usuario-correo", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    List<Usuario> busquedaUsuarioCorreo(@RequestBody Usuario usuario){
        return servicio.busquedaUsuarioCorreo(usuario);
    }
}
