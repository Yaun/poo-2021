export interface Usuario{
  id_usuario?: number;
  nombres?: String;
  apellidos?: String;
  correo?: String;
  administrador?: Boolean;
  clave?: String;
  }

export interface UsuarioLogin{
  email?:string;
  contraseña?:string;
}
