import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApiService } from '../Apiservice';
import { UsuarioLogin, Usuario } from '../interface';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {
  error: string = "";
  form?: FormGroup;
  email: string = '';
  password: string = '';
  mensaje: string = '';
  usuario?: Usuario;
  usuarioLogin?: UsuarioLogin;
  constructor(private api: ApiService ) { }

  ngOnInit(): void {
  }

  ingresar(): void{
    let usuarioLogin: UsuarioLogin;

    usuarioLogin = {
      email: this.email,
      contraseña: this.password
    }

    this.api.verificarUsuario(usuarioLogin).subscribe( data => {
      if(data){
        this.usuario=data;
      }
    },err => {this.mensaje = err;});
  }
}
